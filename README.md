# Horus

### Twitter clone, API version, Golang playground

This is a simple playground project for me. I'm building a Twitter clone using Golang with the [Gin Framework](https://github.com/gin-gonic/gin) for routing requests and [Gorm](gorm.io) as an ORM.

As soon as I get something working I'll keep updating this file (Pffft, _yeah sure_...)