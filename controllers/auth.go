package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/hslzr/horus/db"
	"github.com/hslzr/horus/models"
)

type LoginData struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"username" json:"username" binding:"required"`
}

type Auth struct{}

func (s *Auth) Login(c *gin.Context) {
	var loginData LoginData
	if err := c.Bind(&loginData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User
	if err := db.Conn.Where("handle = ?", loginData.Username).First(&user).Error; err != nil {
		c.AbortWithStatus(404)
		c.JSON(404, err)
		c.Abort()
	} else {
		c.JSON(http.StatusOK, user)
	}
}
