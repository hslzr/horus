package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/hslzr/horus/db"
	"github.com/hslzr/horus/models"
)

type Users struct{}

func (u *Users) Index(c *gin.Context) {
	var users []models.User
	db.Conn.Find(&users)
	c.JSON(200, &users)
}
