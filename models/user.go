package models

import (
	"fmt"

	"github.com/hslzr/horus/auth"
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Name     string `json:"name"`
	Email    string `json:"email"`
	Handle   string `json:"handle"`
	Password string `json:"password"`
	Mips     []Mip
}

func (u *User) BeforeCreate(scope *gorm.Scope) error {
	fmt.Println("About to start HOOK...")
	hashedPass, err := auth.HashPassword(u.Password)
	if err != nil {
		fmt.Println("Your error is: ", err)
		return err
	}
	scope.SetColumn("Password", hashedPass)
	fmt.Println("Done with HOOK...")
	return err
}
