package models

import (
	"github.com/jinzhu/gorm"
)

type Mip struct {
	gorm.Model
	Body   string
	UserID int
}
