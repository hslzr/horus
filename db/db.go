package db

import (
	"fmt"
	"os/user"

	"github.com/hslzr/horus/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var Conn *gorm.DB

func InitDB() {
	var err error
	usr, _ := user.Current()
	dir := usr.HomeDir
	Conn, err = gorm.Open("sqlite3", dir+"/.horus.db")
	if err != nil {
		fmt.Println("Error:", err)
		panic("failed to connect to database")
	}
	Conn.LogMode(true)
}

func MigrateDB(models ...interface{}) {
	Conn.AutoMigrate(models...)
}

func SeedDB() {
	seedUser := &models.User{
		Name:     "Osiris",
		Email:    "osiris@mail.com",
		Handle:   "osiris",
		Password: "demodemo",
	}
	Conn.FirstOrCreate(&seedUser, seedUser)
}
