package main

import (
	"github.com/gin-gonic/gin"
	"github.com/hslzr/horus/controllers"
	"github.com/hslzr/horus/db"
	"github.com/hslzr/horus/models"
)

func main() {
	db.InitDB()
	defer db.Conn.Close()

	db.MigrateDB(&models.Mip{}, &models.User{})
	db.SeedDB()

	server := gin.Default()

	v1 := server.Group("/v1")
	{
		// Auth
		auth := new(controllers.Auth)
		v1.POST("/login", auth.Login)

		// Users
		users := new(controllers.Users)
		v1.GET("/users", users.Index)
	}

	server.Run()
}
